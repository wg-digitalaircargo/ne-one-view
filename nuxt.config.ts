// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  ssr: false,
  modules: [
    '@unocss/nuxt',
    '@element-plus/nuxt',
    'nuxt-icon'
  ],
  css: [
      '@unocss/reset/tailwind-compat.css'
    //'~/assets/main.css'
  ],
  runtimeConfig: {
    public: {
      apiBase: 'http://localhost:8080'
    }
  }
})
