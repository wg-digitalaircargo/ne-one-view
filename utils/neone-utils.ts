// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import {NamedNode} from "rdflib";
import type {LogisticsObjectInfo} from "~/types";

const config = useRuntimeConfig();
const localLoBase = config.public.apiBase + '/logistics-objects/'

const NEONETotalCountHeader = 'NEONE-Total-Count' as const

const headers = {
    NEONETotalCountHeader
}

function truncateLocalLoId(loId: string) {
    return loId.substring(localLoBase.length, loId.length)
}

function findTypes(loInfos: LogisticsObjectInfo[]) {
    return loInfos.filter(value => value.predicate.uri === CargoUtil.RDF.TYPE.value)
        .map(value => {
            const objectNode = value.object as NamedNode
            return objectNode.id()
        })
}

function findLoPredicates(loInfos: LogisticsObjectInfo[]) {
    return loInfos.filter(value => value.predicate.uri !== CargoUtil.RDF.TYPE.value)
}

async function loadAllLos(limit: number, offset: number, type: NamedNode) {
    return fetch(`${config.public.apiBase}/logistics-objects/internal/_all?` +
        new URLSearchParams({limit: limit.toString(), offset: offset.toString(), t: type.value})
    )
}

export const NeoneUtil = {
    headers,
    truncateLocalLoId,
    loadAllLos,
    findTypes,
    findLoPredicates
}