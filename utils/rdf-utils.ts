// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import type {Quad} from "rdflib/lib/tf-types";
import type {Triple} from "~/types";
import {NamedNode} from "rdflib";
import {NamedNodeTermType} from "rdflib/lib/types";
import {nanoid} from "nanoid";

export function toTriple(quad: Quad) : Triple {
    return {
        id: nanoid(),
        subject: quad.subject.value,
        predicate: quad.predicate.value,
        predicateShort: quad.predicate.value.split('#')[1], // TODO: is this always the case? RDF4J handles more cases
        object: quad.object.value,
        isLink: quad.object.termType === NamedNodeTermType &&
            quad.predicate.termType === NamedNodeTermType &&
            CargoUtil.CARGO.NS.doc().sameTerm((quad.predicate as NamedNode).doc()) &&
            !CargoUtil.isNamedIndividual(quad.object as NamedNode),
        isDocumentLink: quad.predicate.equals(CargoUtil.CARGO.DOCUMENT_LINK)
    } as Triple
}
