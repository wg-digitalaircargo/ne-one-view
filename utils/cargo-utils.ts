// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import {defaultGraph, fromNT, graph, NamedNode, Namespace, parse, sym} from "rdflib";
import cargoOntology from "~/utils/cargo-ontology";
import cclOntology from "~/utils/ccl-ontology";
import {NamedNodeTermType} from "rdflib/lib/types";

const CARGO_NS = Namespace('https://onerecord.iata.org/ns/cargo#')
const CARGO = {
    NS: sym(CARGO_NS('').value),
    LOGISTICS_OBJECT : sym(CARGO_NS('LogisticsObject').value),
    DOCUMENT_LINK: sym(CARGO_NS('documentLink').value)
}

const RDFS_NS = Namespace('http://www.w3.org/2000/01/rdf-schema#')
const RDFS = {
    NS: sym(RDFS_NS('').value),
    RANGE: sym(RDFS_NS('range').value)
}

const RDF_NS = Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
const RDF = {
    NS: sym(RDF_NS('').value),
    TYPE: sym(RDF_NS('type').value)
}

const OWL_NS = Namespace("http://www.w3.org/2002/07/owl#")
const OWL = {
    NS: sym(OWL_NS('').value),
    NI: sym(OWL_NS('NamedIndividual').value)
}

const kb = graph()
parse(cargoOntology, kb, defaultGraph().uri)
parse(cclOntology, kb, defaultGraph().uri)

function referencesLogisticsObject(predicate: NamedNode): boolean {
    const type = kb.statementsMatching(predicate, RDFS.RANGE, null)
        .map(value => value.object)
        .filter(object  => object.termType === NamedNodeTermType)
        .find(namdNode => kb.findSuperClassesNT(namdNode)[CARGO.LOGISTICS_OBJECT.toNT()])

    return type !== undefined
}

function getLogisticsObjectTypes(): NamedNode[] {
    let dict = kb.findSubClassesNT(CARGO.LOGISTICS_OBJECT);
    for (const key in dict) {
        sym(key)
    }
    return Object.keys(dict).map(value => sym(fromNT(value)))
}

function isNamedIndividual(node: NamedNode): boolean {
    return (kb.statementsMatching(node, RDF.TYPE, OWL.NI).length > 0)
}

export const CargoUtil = {
    CARGO,
    RDFS,
    RDF,
    OWL,
    referencesLogisticsObject,
    isNamedIndividual,
    getLogisticsObjectTypes
}
