// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

import {NamedNode} from "rdflib";
import {type ObjectType} from "rdflib/lib/types";

export interface Triple {
    id: string
    subject: string
    predicate: string
    predicateShort: string
    object: string
    isLink?: boolean
    linkedTriples?: Triple[]
    isDocumentLink: boolean
}

export interface LogisticsObjectInfo {
    predicate: NamedNode,
    object: ObjectType
}

export interface LogisticsObjectDict {
    [index: string]: LogisticsObjectInfo[]
}